use std::io;
use std::collections::HashSet;

macro_rules! parse_input {
    ($x:expr, $t:ident) => ($x.trim().parse::<$t>().unwrap()) 
}

use std::hash::{Hash, Hasher};

#[derive(PartialEq, Debug, Clone)]
struct Bridge {
    path_1: i32,
    path_2: i32,
    line_nbr: usize
}

impl Hash for Bridge {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.path_1.hash(state);
        self.path_2.hash(state);
        self.line_nbr.hash(state);
    }
}

impl Eq for Bridge {}

fn main() {
    let mut input_line = String::new();
    io::stdin().read_line(&mut input_line).unwrap();

    let inputs = input_line.split(" ").collect::<Vec<_>>();
    let _w = parse_input!(inputs[0], i32);
    let h = parse_input!(inputs[1], i32);

    let mut bridge = HashSet::new();
    let mut top= vec![];
    let mut bottom = vec![];

    for i in 0..h as usize {
        let mut input_line = String::new();

        io::stdin().read_line(&mut input_line).unwrap();

        let line = input_line.trim_end().to_string();
        if i == 0 as usize {
            top = line.split_whitespace().map(|s| s.to_owned()).collect::<Vec<String>>();
        } else if i == (h -1) as usize {
            bottom = line.split_whitespace().map(|s| s.to_owned()).collect::<Vec<String>>();
        }
        else {
            save_paths(&line, i, &mut bridge);
        }
    }

    find_path(top, bottom, bridge, (h - 1) as usize);
}

fn find_path(
    top: Vec<String>,
    bottom: Vec<String>,
    briges: HashSet<Bridge>,
    h: usize
) {
    for (start_column, _) in top.iter().enumerate() {
        cross_path(
            &top,
            &bottom,
            &briges,
            start_column as i32,
            h
        );
    }
}

fn cross_path(
    top: &Vec<String>,
    bottom: &Vec<String>,
    briges: &HashSet<Bridge>,
    start_column: i32,
    h: usize
) {

    let mut end_column = start_column;

    for line_nbr in 1..h as usize {
        match get_bridge(end_column, line_nbr, &briges) {
            (None, Some(right)) => {
                end_column = right.path_2;
            },
            (Some(left), _) => {
                end_column = left.path_1;
            },
            _ => {}
        };
    }

    println!("{}{}", top[start_column as usize], bottom[end_column as usize])
}

fn get_bridge<'a>(
    column: i32,
    line_nbr: usize,
    bridges: &'a HashSet<Bridge>
) -> (Option<&'a Bridge>, Option<&'a Bridge>) {

    let left = bridges.get(
        &Bridge {
            path_1: column - 1,
            path_2: column,
            line_nbr
        }
    );

    let right = bridges.get(
        &Bridge {
            path_1: column,
            path_2: column + 1,
            line_nbr
        }
    );

    (left, right)
}

fn save_paths(
    line: &str,
    line_nbr: usize,
    bridges: &mut HashSet<Bridge>
) {
    let bridge: Vec<(usize, &str)> = line.rmatch_indices("--").collect();
    let mut paths: Vec<(usize, &str)> = line.rmatch_indices('|').collect();

    paths.reverse();

    for (i_b, _) in bridge.iter() {
        for (index, (i_p, _)) in paths.iter().enumerate() {

            if i_p > i_b {
                bridges.insert(
                    Bridge {
                        path_1: (index - 1) as i32,
                        path_2: index as i32,
                        line_nbr,
                    }
                );
                break;
            }
        }
    }
}